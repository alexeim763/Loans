class PaymentPdf < Prawn::Document
	def initialize(loan, totalAmortization, totalInterest , totalFee)
		super(top_margin: 35)
		@loan = loan
		@user = @loan.user
		@totalAmortization = totalAmortization
		@totalInterest = totalInterest
		@totalFee = totalFee

		@payments = @loan.payments
		@number = 0
		main_title
		text_dateUser
		line_items
		line_totalText
	end
	def text_dateUser
		text "Nombre : #{@user.firstName}", size: 12 , style: :bold, :align => :left
		text "Apellidos : #{@user.lastName}" , size: 12 , style: :bold, :align => :left
		text "Prestamo : S/. #{@loan.amount} " , size: 12 , style: :bold, :align => :left
	end
	def main_title
		text "Tabla de Amortizaciones", size: 25 , style: :bold, :align => :center
	end
	def line_items
		move_down 20
		table item_table_data do
			row(0).font_style = :bold
			self.header = true
		end
	end

	def item_header
  	["Número", "Fecha de Pago","Saldo inicial", "Amortización","Intereres", "Cuota" , "Saldo Final"]
	end
	def item_rows
	  @payments.map { |payment|
	  	[@number += 1, payment.datePay,payment.inicialBalance, payment.amortization, payment.interest , payment.fee , payment.finalBalance]
	  }
	end
  def item_table_data
  	[item_header, *item_rows]
  end

  def line_totalText
  	move_down 15
  	text "Total de Amortizacion : S/. #{@totalAmortization}", size: 10 , :align => :left
  	text "Total de Interes : S/. #{@totalInterest}", size: 10 , :align => :left
  	text "Total de Saldo Final : S/. #{@totalFee}", size: 10 , :align => :left
  end

end