json.array!(@loans) do |loan|
  json.extract! loan, :id, :people_id, :dateLoan, :amount, :dailyInterest, :term, :detail
  json.url loan_url(loan, format: :json)
end
