json.array!(@payments) do |payment|
  json.extract! payment, :id, :user_id, :loan_id, :datePay, :inicialBalance, :amortization, :interest, :fee, :finalBalance
  json.url payment_url(payment, format: :json)
end
