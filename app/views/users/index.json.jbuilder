json.array!(@users) do |user|
  json.extract! user, :id, :firstName, :lastName, :address, :phone, :dni, :totalDebt
  json.url user_url(user, format: :json)
end
