class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy, :pay]
  before_action :set_person
  before_action :set_loan
  before_action :authenticate_admin!

  # GET /payments
  # GET /payments.json
  def index
    @payments = @loan.payments
    @quantity = @loan.payments.count
    @totalAmortization = @loan.payments.sum(:amortization).round(2)
    @totalInterest = @loan.payments.sum(:interest).round(2)
    @totalFee = @loan.payments.sum(:fee).round(2)

    respond_to do |format|
      format.html
      format.pdf do
        pdf = PaymentPdf.new(@loan, @totalAmortization , @totalInterest , @totalFee ) 
        send_data pdf.render, filename: "Pago_#{@loan.user.firstName}.pdf",
                              type: "aplication/pdf",
                              disposition: "inline"

      end
    end

  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end

  def pay
    if @payment.update(:state => 1)
        redirect_to user_loan_payments_path
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
  end
  # POST /payments
  # POST /payments.json
  def create
    @days = @loan.term
    @interest = @loan.dailyInterest/100
    @amount = @loan.amount
    @cuot = ((@interest * @amount) / (1 - ((1/(1+@interest)))**@days))
    @cuotRound = @cuot.round(2)
    @inicialBalance = @amount
    @datePay = @loan.dateLoan
    
    
    @days.times{
      @payment = Payment.new


      @interestAmount = (@inicialBalance * @interest) .round(2)
      @amortization = (@cuotRound - @interestAmount).round(2)
      @finalBalance = (@inicialBalance - @amortization).round(2)
      
      @payment.fee = @cuotRound
      @payment.inicialBalance = @inicialBalance
      @payment.interest = @interestAmount
      @payment.amortization = @amortization
      @payment.finalBalance = @finalBalance
      @payment.datePay = @datePay

      @payment.user = @user
      @payment.loan = @loan
      @payment.save

      @inicialBalance = @finalBalance
      @datePay = @datePay + 1
    }
    
    redirect_to user_loan_payments_path
    

  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end
    def set_loan
      @loan = Loan.find(params[:loan_id])
    end
    def set_person
      @user = User.find(params[:user_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      #params.require(:payment).permit(:user_id, :loan_id, :datePay, :inicialBalance, :amortization, :interest, :fee, :finalBalance)
    end
end
