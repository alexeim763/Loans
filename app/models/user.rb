class User < ActiveRecord::Base
	has_many :loans
	has_many :payments
	before_save :default_values

	private
	def default_values
		unless self.totalDebt 
			self.totalDebt = 0
		end
	end
end
