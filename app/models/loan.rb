class Loan < ActiveRecord::Base
  belongs_to :user
  has_many :payments
  before_save :add_mountUser

  private
  def add_mountUser
  	@usuario = self.user
  	@newDebt = @usuario.totalDebt + self.amount
  	@usuario.totalDebt = @newDebt
  	@usuario.save!
  end
end
