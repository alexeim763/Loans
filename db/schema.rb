# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150724020209) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "loans", force: :cascade do |t|
    t.integer  "user_id",          limit: 4
    t.date     "dateLoan"
    t.float    "amount",           limit: 24
    t.float    "dailyInterest",    limit: 24
    t.integer  "term",             limit: 4
    t.text     "detail",           limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "typeLoan",         limit: 255
    t.string   "typeAmortization", limit: 255
  end

  add_index "loans", ["user_id"], name: "index_loans_on_people_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id",        limit: 4
    t.integer  "loan_id",        limit: 4
    t.date     "datePay"
    t.float    "inicialBalance", limit: 24
    t.float    "amortization",   limit: 24
    t.float    "interest",       limit: 24
    t.float    "fee",            limit: 24
    t.float    "finalBalance",   limit: 24
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "state",          limit: 4
  end

  add_index "payments", ["loan_id"], name: "index_payments_on_loan_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "firstName",  limit: 255
    t.string   "lastName",   limit: 255
    t.string   "address",    limit: 255
    t.string   "phone",      limit: 255
    t.string   "dni",        limit: 255
    t.float    "totalDebt",  limit: 24
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
