class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user, index: true, foreign_key: true
      t.references :loan, index: true, foreign_key: true
      t.date :datePay
      t.float :inicialBalance
      t.float :amortization
      t.float :interest
      t.float :fee
      t.float :finalBalance

      t.timestamps null: false
    end
  end
end
