class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstName
      t.string :lastName
      t.string :address
      t.string :phone
      t.string :dni
      t.float :totalDebt

      t.timestamps null: false
    end
  end
end
